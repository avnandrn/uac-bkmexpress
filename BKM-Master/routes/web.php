<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () { return view('index'); });

Route::get('/api/login', function () { return view('login'); });
Route::get('/api/isLogin','AuthenticationController@isLogin')->name('check.login');

Route::post('/api/login','AuthenticationController@login')->name('post.login');

Route::group(['middleware'=>'check.login'],function(){
	Route::get('/home', 'HomeController@index')->name('home');

	//users
	Route::get('/users', 'UserController@index')->name('users');
	Route::post('/users', 'UserController@store')->name('users.store');
	Route::post('/user/{id}', 'UserController@update')->name('users.update');
	Route::delete('/user/{id}', 'UserController@delete')->name('users.delete');
	Route::get('/data/users', 'UserController@data')->name('users.data');
});

Route::middleware('auth:api')->group(function () {
    Route::get('api/logout','AuthenticationController@logout')->name('logout');
});


