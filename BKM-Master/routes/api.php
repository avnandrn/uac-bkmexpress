<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Public Route
// Route::get('/login','AuthenticationController@isLogin')->name('isLogin');
// Route::get('/login', function () { return view('login'); });
// Route::get('/isLogin','AuthenticationController@isLogin')->name('check.login');

// Route::post('/login','AuthenticationController@login')->name('post.login');

// Private Route
// Route::middleware('auth:api')->group(function () {
//     Route::get('/logout','AuthenticationController@logout')->name('logout');
// });


