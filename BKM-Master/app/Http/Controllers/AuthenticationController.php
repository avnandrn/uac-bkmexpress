<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Lcobucci\JWT\Parser;
use App\User;

class AuthenticationController extends Controller
{
    //

		public function isLogin(Request $request){
				$response = ['login' => false, 'token' => session('accessToken')];

				if ($request->session()->exists('accessToken')) {
	        	$response = ['login' => true, 'token' => session('accessToken')];
	      }

        return response($response, 200);
		}

		public function login(Request $request) {

				$this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

	      $user = User::where('email', $request->email)->first();
	      
	      if ($user) {
	          if (\Hash::check($request->password, $user->password)) {
	              $token = $user->createToken('Laravel Password Grant Client')->accessToken;
	              session(['accessToken' => $token]);
	              $response = ['token' => $token];

	              return response($response, 200);
	          } else {
	              $response = ['type' => "password", 'msg' => 'Password mismatch.'];
	              return response($response, 422);
	          }
	      } else {
            $response = ['type' => "email", 'msg' => 'User doesn\'t exist.'];
	          return response($response, 422);
	      }
	      
	  }

	  public function logout(Request $request) {
	      $value = $request->bearerToken();
	      $id= (new Parser())->parse($value)->getHeader('jti');
	      $token= $request->user()->tokens->find($id);
	      $token->revoke();
	      $request->session()->forget('accessToken');
	      $response = 'You have been successfully logged out!';
	      return response($response, 200);
	  }
}
