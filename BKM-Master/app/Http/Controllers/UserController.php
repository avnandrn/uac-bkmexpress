<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Exception;
use App\User;

class UserController extends Controller
{
    //
		public function index($value='')
		{
			# code...
			return view('users');
		}

		public function data()
		{
			# code...
			$users = User::get();

			$data['users'] = $users;

			return response()->json([
          'result'=>$data,
      ]);
		}

		public function store(Request $request)
		{
			# code...
			$request->validate([
          'name' => 'required',
          'email' => 'required',
          'password' => 'required|confirmed',
      ]);

      DB::beginTransaction();
	    try {

	      User::create(['name'=>$request->name,'email'=>$request->email,'password'=>bcrypt($request->password)]);

	      DB::commit();

	      return response()->json([
	        'code' => "200",
	        'msg' => "User has been added successfully.",
	      ]);

	    } catch (Exception $e) {
	      DB::rollback();
	      return response()->json([
	        'code' => $e->getCode(),
	        'msg' => $e->getMessage(),
	      ]);
	    }
		}

		public function update($id,Request $request)
		{
			# code...
			$request->validate([
          'name' => 'required',
          'email' => 'required',
          'password' => 'required|confirmed',
      ]);

      DB::beginTransaction();
	    try {

	      $data = User::where('id',$id)->first();

	      if($data){
	      	$data->name = $request->name;
	      	$data->email = $request->email;
	      	$data->password = bcrypt($request->password);
	      	$data->save();
	      }else{
	      	throw new Exception("User data not found.", 400);
	      }

	      DB::commit();

	      return response()->json([
	        'code' => "200",
	        'msg' => "User has been updated successfully.",
	      ]);

	    } catch (Exception $e) {
	      DB::rollback();
	      return response()->json([
	        'code' => $e->getCode(),
	        'msg' => $e->getMessage(),
	      ]);
	    }
		}

		public function delete($id)
		{
			# code...
			$data = User::where('id',$id)->first();

			if($data){
				$data->delete();
			}else{
				return response()->json([
	        'code' => "400",
	        'msg' => "User data not found.",
	      ]);
			}

			return response()->json([
        'code' => "200",
        'msg' => "User has been deleted successfully.",
      ]);
		}
}
