<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie" lang="{{ app()->getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Open Sans font from Google CDN -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

    <!-- Pixel Admin's stylesheets -->
    <link href="{{ asset('pixel-admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('pixel-admin/css/pixel-admin.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('pixel-admin/css/pages.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('pixel-admin/css/rtl.min.css') }}"" rel="stylesheet" type="text/css'">
    <link href="{{ asset('pixel-admin/css/themes.min.css') }}" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script src="assets/javascripts/ie.min.js"></script>
    <![endif]-->
</head>
<body class="theme-default page-signin-alt">
    <div id="app">
        @yield('content')
    </div>

    <script src="{{ asset('pixel-admin/js/jquery.min.js') }}"></script>

    <!-- Pixel Admin's javascripts -->
    <script src="{{ asset('pixel-admin/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('pixel-admin/js/pixel-admin.min.js') }}"></script>
</body>
</html>
