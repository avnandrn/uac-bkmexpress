let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .copy(['resources/assets/stylesheets/bootstrap.min.css',
        'resources/assets/stylesheets/pixel-admin.min.css',
        'resources/assets/stylesheets/pages.min.css',
        'resources/assets/stylesheets/widgets.min.css',
        'resources/assets/stylesheets/rtl.min.css',
        'resources/assets/stylesheets/themes.min.css'], 'public/pixel-admin/css')
	 .copy(['resources/assets/javascripts/bootstrap.min.js',
	 			'resources/assets/javascripts/pixel-admin.min.js',
	 			'node_modules/jquery/dist/jquery.min.js'],'public/pixel-admin/js')
	 .copy('resources/assets/fonts', 'public/pixel-admin/fonts')
   .copy(['resources/assets/images/logo.png',
        'resources/assets/demo/signin-bg-1.jpg',
        'resources/assets/demo/avatars/1.jpg'], 'public/img');
